package coursework.abdiyee.ManorFarm;

import org.newdawn.slick.opengl.Texture;
import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

import GraphicsLab.Colour;
import GraphicsLab.FloatBuffer;
import GraphicsLab.Normal;
import GraphicsLab.Vertex;

public class Fence extends FarmItem {

	private Texture fenceTexture;

	public Fence(int listLocaion, Texture fenceTexture) {
		super(listLocaion);

		// set the fence entire
		this.fenceTexture = fenceTexture;
		GL11.glNewList(this.getNum(), GL11.GL_COMPILE);
		{
			// draw left fence
			GL11.glPushMatrix();
			{
				// enable textures
				// draw a 30,30 fence
				GL11.glEnable(GL11.GL_TEXTURE_2D);
				// add the wood texture
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, fenceTexture.getTextureID());
				// draw the fence
				drawFullFence(30);

			}
			GL11.glPopMatrix();
		}
		GL11.glEndList();

	}

	private void drawFullFence(int fenceLength) {
		for (int i = 0; i < 4; i++) {
			// rotate it by Y by 90 to draw the next row
			GL11.glRotatef(90, 0, 1, 0);

			for (int j = 0; j < fenceLength; j++) {
				
				GL11.glPushMatrix();
				{
					// draw a fence (row of crosses)
					drawFence();
					GL11.glTranslatef(0, 2.5f, 0);
					
					// and draw another below it
					drawFence();

				}
				GL11.glPopMatrix();
				// draw the next 2 crosses 1.5 away from the current cross 
				GL11.glTranslatef(1.5f, 0, 0);
			}
		}

	}

	private void drawFence() {
		/*
		 * For each cross
		 * 	Draw the front vectors manually
		 * 	Draw the back using a loop vectors which is the front vector with z-depth
		 * 	Also set the glTexCoor so that the textures can be mapped on
		 * 	Also set the normals so that it can lit accordingly
		 * */
		ArrayList<Vertex> fenceVertex = new ArrayList<Vertex>();

		// the vertices for the cube (note that all sides have a length of 1)
		fenceVertex.add(new Vertex(-0.25f, -1.25f, 0f));
		fenceVertex.add(new Vertex(-0.25f, -0.25f, 0f));
		fenceVertex.add(new Vertex(-0.75f, -0.25f, 0f));
		fenceVertex.add(new Vertex(-0.75f, 0.25f, 0f));
		fenceVertex.add(new Vertex(-0.25f, 0.25f, 0f));
		fenceVertex.add(new Vertex(-0.25f, 1.25f, 0f));
		fenceVertex.add(new Vertex(0.25f, 1.25f, 0));
		fenceVertex.add(new Vertex(0.25f, 0.25f, 0));
		fenceVertex.add(new Vertex(0.75f, 0.25f, 0));
		fenceVertex.add(new Vertex(0.75f, -0.25f, 0));
		fenceVertex.add(new Vertex(0.25f, -0.25f, 0));
		fenceVertex.add(new Vertex(0.25f, -1.25f, 0));

		// add the vertex for the back of the cross as it's just the front with a reduced Y
		float depth = -0.25f;
		for (int i = 0; i < 12; i++) {
			fenceVertex.add(new Vertex(fenceVertex.get(i).getX(), fenceVertex.get(i).getY(), depth));
		}

		GL11.glBegin(GL11.GL_POLYGON);
		{
			// enable lighting for given vectors
			new Normal(fenceVertex.get(0).toVector(), fenceVertex.get(12).toVector(),
					fenceVertex.get(13).toVector(), fenceVertex.get(11).toVector()).submit();
			GL11.glTexCoord2f(0f, 0.0f);
		
			fenceVertex.get(0).submit();

			GL11.glTexCoord2f(1.0f, 0.0f);
			fenceVertex.get(12).submit();
			GL11.glTexCoord2f(1.0f, 1.0f);
			fenceVertex.get(23).submit();
			GL11.glTexCoord2f(0.0f, 1.0f);
			fenceVertex.get(11).submit();
		}
		GL11.glEnd();

		// connect each vertex using a loop
		int fv1 = 0;
		int bv1 = 12;
		int bv2 = 13;
		int fv2 = 1;
		for (int i = 0; i < 11; i++) {

			GL11.glBegin(GL11.GL_POLYGON);
			{
				// add normals for lighting
				new Normal(fenceVertex.get(fv1).toVector(), fenceVertex.get(bv1).toVector(),
						fenceVertex.get(bv2).toVector(), fenceVertex.get(fv2).toVector()).submit();

				GL11.glTexCoord2f(0f, 0.0f);
				fenceVertex.get(fv1).submit();

				GL11.glTexCoord2f(1.0f, 0.0f);
				fenceVertex.get(bv1).submit();
				GL11.glTexCoord2f(1.0f, 1.0f);
				fenceVertex.get(bv2).submit();
				GL11.glTexCoord2f(0.0f, 1.0f);
				fenceVertex.get(fv2).submit();
			}
			GL11.glEnd();

			fv1++;
			bv1++;
			bv2++;
			fv2++;
		}

		// colour the middle of the shape

		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(fenceVertex.get(0).toVector(), fenceVertex.get(5).toVector(), fenceVertex.get(6).toVector(),
					fenceVertex.get(11).toVector()).submit();
			GL11.glTexCoord2f(0f, 0.0f);
			fenceVertex.get(0).submit();
			GL11.glTexCoord2f(0f, 1f);
			fenceVertex.get(5).submit();
			GL11.glTexCoord2f(1f, 1f);
			fenceVertex.get(6).submit();
			GL11.glTexCoord2f(1f, 0f);
			fenceVertex.get(11).submit();

		}
		GL11.glEnd();

		// now the cross 
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(fenceVertex.get(2).toVector(), fenceVertex.get(3).toVector(), fenceVertex.get(8).toVector(),
					fenceVertex.get(9).toVector()).submit();

			GL11.glTexCoord2f(0f, 0.0f);

			fenceVertex.get(2).submit();
			GL11.glTexCoord2f(0f, 1f);
			fenceVertex.get(3).submit();
			GL11.glTexCoord2f(1f, 1f);
			fenceVertex.get(8).submit();
			GL11.glTexCoord2f(1f, 0f);
			fenceVertex.get(9).submit();

		}
		GL11.glEnd();
		
		// color the middle
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(fenceVertex.get(12).toVector(), fenceVertex.get(17).toVector(), fenceVertex.get(19).toVector(),
					fenceVertex.get(23).toVector()).submit();

			GL11.glTexCoord2f(0f, 0.0f);
			fenceVertex.get(12).submit();
			GL11.glTexCoord2f(0f, 1f);
			fenceVertex.get(17).submit();
			GL11.glTexCoord2f(1f, 1f);
			fenceVertex.get(19).submit();
			GL11.glTexCoord2f(1f, 0f);
			fenceVertex.get(23).submit();

		}
		GL11.glEnd();

		// now the cross

		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(fenceVertex.get(14).toVector(), fenceVertex.get(15).toVector(), fenceVertex.get(20).toVector(),
					fenceVertex.get(21).toVector()).submit();

			GL11.glTexCoord2f(0f, 0.0f);

			fenceVertex.get(14).submit();
			GL11.glTexCoord2f(0f, 1f);
			fenceVertex.get(15).submit();
			GL11.glTexCoord2f(1f, 1f);
			fenceVertex.get(20).submit();
			GL11.glTexCoord2f(1f, 0f);
			fenceVertex.get(21).submit();

		}
		GL11.glEnd();

	}

	public void draw() {
		// the fence is WOODEN 
		// set the lighting
		float shininess = 0.2f; // wood is not shiny
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float diffuse[] = {0.8235f, 0.7058f, 0.5490f, 1.0f }; // make the wood light brown
		// set the material the wood
		GL11.glMaterialf(GL11.GL_FRONT, GL11.GL_SHININESS, shininess);
		GL11.glMaterial(GL11.GL_FRONT, GL11.GL_SPECULAR, FloatBuffer.wrap(specular));
		GL11.glMaterial(GL11.GL_FRONT, GL11.GL_DIFFUSE, FloatBuffer.wrap(diffuse));
		GL11.glMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT, FloatBuffer.wrap(diffuse));
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_NORMALIZE);

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, fenceTexture.getTextureID());
		GL11.glCallList(this.getNum());
	}

}
