package coursework.abdiyee.ManorFarm;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Sphere;
import org.newdawn.slick.opengl.Texture;

import GraphicsLab.*;

public class Tractor extends FarmItem {

	float x;
	float y;
	float z;

	// records start position of the tractor
	float originalX;
	float originalY;
	float originalZ;

	// the speed of the tractor
	public static final float SPEED = 0.05f;

	int direction = 1;

	public Tractor(int listLocation, float x, float y, float z, Texture tractorTexture, Texture wheelTexture,
			Texture tractorFrontTexture) {
		super(listLocation);

		this.x = x;
		this.y = y;
		this.z = z;

		this.originalX = x;
		this.originalY = y;
		this.originalZ = z;

		// for the lighting

		// create the list of items
		GL11.glNewList(this.getNum(), GL11.GL_COMPILE);
		{
			GL11.glPushAttrib(GL11.GL_TEXTURE);
			{

				// enable texturing
				GL11.glEnable(GL11.GL_TEXTURE_2D);

				// apply the texture for the back which is green plastic
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, tractorTexture.getTextureID());

				// draw front
				drawBox(0.5f, 0.5f, 0.5f); // draw back driver part (A)

				// move the front infront of the back
				GL11.glTranslatef(0.0f, -0.2f, -0.9f);

				// bind the red texture
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, tractorFrontTexture.getTextureID());

				drawBox(0.3f, 0.2f, 0.4f); // draw engine part (B)

				// draw left leg for front left wheel
				GL11.glTranslatef(-0.4f, -0.1f, -0.1f);
				drawBox(0.05f, -0.2f, 0.05f);

				// draw right leg for front right wheel
				GL11.glTranslatef(0.8f, 0.0f, -0.1f);
				drawBox(0.05f, -0.2f, 0.05f);

				// bind the rubber texture
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, wheelTexture.getTextureID());

				// draw the wheels
				// the two front wheels
				drawWheel(0.1f, -0.2f, 0f, 1.5f);
				// front right wheel
				drawWheel(-0.9f, -0.2f, 0.1f, 1.5f);
				// back wheels left
				drawWheel(-0.9f, -0.2f, 1.1f, 3f);
				// back wheels right
				drawWheel(0.1f, -0.2f, 1.1f, 3f);
			}

			GL11.glPopAttrib();

		}
		GL11.glEndList();

	}

	// move the tractor back to it's starting position
	public void reset() {
		x = this.originalX;
		y = this.originalY;
		z = this.originalZ;
		direction = 1;
	}
	// change the tractors direction
	public void flipDirection() {
		this.direction = direction * -1;
	}
	
	// change the z in the direction of the tractor
	public void moveForward() // move the z by given amount
	{
		this.z = this.z + (SPEED * Farm.ANIMATIONSCALE) * direction;
	}
	
	// draw a wheel
	public void drawWheel(float x, float y, float z, float size) {

		GL11.glPushMatrix();
		{
			// draw right wheel using given coordinates
			GL11.glTranslatef(x, y, z);
			// rotate it
			GL11.glRotatef(90, 0, 1, 0);
			// make the sphere bigger
			GL11.glScalef(0.14f, 0.14f, 0.05f);

			Sphere sphere = new Sphere();
			sphere.draw(size, 10, 3);
		}
		GL11.glPopMatrix();

	}

	/*
	 * getters and setters for the fields
	 */
	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float getX() {
		return this.x;
	}

	public float getY() {
		return this.y;
	}

	public float getZ() {
		return this.z;
	}

	public void draw() {
		// add the lighting for the tractor
		// set the lighting
		float shininess = 20f; // metal and lighting is very shiny
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		// set the material properties for the house using OpenGL
		GL11.glMaterialf(GL11.GL_BACK, GL11.GL_SHININESS, shininess);
		GL11.glMaterial(GL11.GL_BACK, GL11.GL_SPECULAR, FloatBuffer.wrap(specular));
		GL11.glMaterial(GL11.GL_BACK, GL11.GL_DIFFUSE, FloatBuffer.wrap(diffuse));
		GL11.glMaterial(GL11.GL_BACK, GL11.GL_AMBIENT, FloatBuffer.wrap(diffuse));
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_NORMALIZE);

		GL11.glCallList(this.getNum());

	}

}
