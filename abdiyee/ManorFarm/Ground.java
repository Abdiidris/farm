
package coursework.abdiyee.ManorFarm;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

import GraphicsLab.Colour;
import GraphicsLab.FloatBuffer;

public class Ground extends FarmItem {

	Texture dirtTexture;
	Texture grassTexture;
	float[] harvestLength; // records the length of each harvest

	public Ground(int itemNumber, Texture dirtTexture, Texture grassTexture) {
		super(itemNumber);

		harvestLength = new float[] { 1f, 1f, 1f, 1f };

		this.grassTexture = grassTexture;

		// create the list of items
		GL11.glNewList(this.getNum(), GL11.GL_COMPILE);
		{
			// draw ground
			GL11.glPushMatrix();
			{
				// rotate so that its flat x by 90*
				// push it forward in the Z axis
				GL11.glTranslatef(-1f, 0, -1f);
				GL11.glRotatef(90, 1, 0, 0);

				// add texture
				GL11.glEnable(GL11.GL_TEXTURE_2D);

				GL11.glBindTexture(GL11.GL_TEXTURE_2D, dirtTexture.getTextureID());

				drawBox(1, 1, 0.1f);

			}
			GL11.glPopMatrix();
		}

		GL11.glEndList();

	}

	// make the size of this grass 0
	public void removeHarvest(int at) {
		harvestLength[at] = 0;
	}

	public void reduceHarvest(int at, float by) {
		harvestLength[at] = harvestLength[at] - by;
	}

	public void drawGrass() {

		// draw ground
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(-1f, 0, -1f);
			GL11.glRotatef(90, 1, 0, 0);

			GL11.glBindTexture(GL11.GL_TEXTURE_2D, grassTexture.getTextureID());

			float x = 0;
			for (int i = 0; i < harvestLength.length; i++) {

				GL11.glPushMatrix();
				{
					// move it up
					GL11.glTranslatef(x, 0, -0.1f);
					// do not draw it if it has been completely harvest
					if (harvestLength[i] != 0) {

						if ((i + 1) % 2 != 0) // every other
						{
							GL11.glTranslatef(-0.8f, (1f - harvestLength[i]), -0.1f); // y= y + amount harvest which is
																						// original -currentLength

						} else {
							// move it up
							GL11.glTranslatef(-0.8f, (harvestLength[i] - 1f), -0.1f); // y= y + amount harvest which is
																						// original -currentLength

						}

						// draw the plants
						drawBox(0.1f, harvestLength[i], 0.2f);
					}

				}
				GL11.glPopMatrix();

				x = x + 0.5f;

			}

		}
		GL11.glPopMatrix();

	}

	@Override
	public void draw() {

		GL11.glPushAttrib(GL11.GL_LIGHTING);

		{
			// set the lighting

			float shininess = 1f; // wood is not shiny
			float specular[] = { 0.5f, 0.5f, 0.5f, 1.0f };
			float diffuse[] = { 0.5450f, 0.2705f, 0.0745f, 1f };
			// set the material properties for the house using OpenGL
			GL11.glMaterialf(GL11.GL_FRONT, GL11.GL_SHININESS, shininess);
			GL11.glMaterial(GL11.GL_FRONT, GL11.GL_SPECULAR, FloatBuffer.wrap(specular));
			GL11.glMaterial(GL11.GL_FRONT, GL11.GL_DIFFUSE, FloatBuffer.wrap(diffuse));
			GL11.glMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT, FloatBuffer.wrap(diffuse));
			GL11.glEnable(GL11.GL_NORMALIZE);
			GL11.glEnable(GL11.GL_LIGHTING);

			GL11.glCallList(this.getNum());
			// lighting for the grass
			float grassShininess = 10f; // wood is not shiny
			float grassSpecular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
			float grassDiffuse[] = { 0f, 1f, 0f, 1f };
			// set the material properties for the house using OpenGL
			GL11.glMaterialf(GL11.GL_FRONT, GL11.GL_SHININESS, grassShininess);
			GL11.glMaterial(GL11.GL_FRONT, GL11.GL_SPECULAR, FloatBuffer.wrap(grassSpecular));
			GL11.glMaterial(GL11.GL_FRONT, GL11.GL_DIFFUSE, FloatBuffer.wrap(grassDiffuse));
			GL11.glMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT, FloatBuffer.wrap(grassDiffuse));

			drawGrass();
		}
		GL11.glPopAttrib();

	}

}
