package coursework.abdiyee.ManorFarm.Designer;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import GraphicsLab.Colour;
import GraphicsLab.FloatBuffer;
import GraphicsLab.Normal;
import GraphicsLab.Vertex;

import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.glu.Sphere;
import org.newdawn.slick.opengl.Texture;

import coursework.abdiyee.ManorFarm.Droplet;
/**
 * The shape designer is a utility class which assits you with the design of a
 * new 3D object. Replace the content of the drawUnitShape() method with your
 * own code to creates vertices and draw the faces of your object.
 * 
 * You can use the following keys to change the view: - TAB switch between
 * vertex, wireframe and full polygon modes - UP move the shape away from the
 * viewer - DOWN move the shape closer to the viewer - X rotate the camera
 * around the x-axis (clockwise) - Y or C rotate the camera around the y-axis
 * (clockwise) - Z rotate the camera around the z-axis (clockwise) - SHIFT keep
 * pressed when rotating to spin anti-clockwise - A Toggle colour (only if using
 * submitNextColour() to specify colour) - SPACE reset the view to its initial
 * settings
 * 
 * @author Remi Barillec
 *
 */
import coursework.abdiyee.ManorFarm.Farm;
import coursework.abdiyee.ManorFarm.FarmItem;
import coursework.abdiyee.ManorFarm.Fence;
import coursework.abdiyee.ManorFarm.Ground;
import coursework.abdiyee.ManorFarm.Rain;
import coursework.abdiyee.ManorFarm.Tractor;

public class ShapeDesigner extends AbstractDesigner {

	/** Main method **/
	Farm farm;

	public static void main(String args[]) {

		new ShapeDesigner().run(WINDOWED, "Designer", 0.01f);

	}

	public ShapeDesigner() {
		super();

	}

	Fence fence;

	@Override
	protected void initScene() throws Exception {

		fence = new Fence(21, loadTexture("coursework/abdiyee/ManorFarm/textures/wood.bmp"));

		super.initScene();

	}

	

	/** Draw the shape **/
	protected void drawUnitShape() {

		GL11.glDisable(GL11.GL_CULL_FACE); // No back face culling
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);

		// position the tractor inside the fence
		// draw the ground plane
		GL11.glNewList(1, GL11.GL_COMPILE);
		{
			GL11.glPushMatrix();
			{
				Colour.WHITE.submit();
				GL11.glTranslated(0, 0, 0);
				fence.draw();
			}
			GL11.glPopMatrix();
		}
		GL11.glEndList();
		
		GL11.glCallList(1);
	}

	@Override
	protected void setSceneCamera() {
		// call graphiclab camera setup
		super.setSceneCamera();


		GLU.gluLookAt(-20.0f, 40.0f, 47.0f, // viewer location
				-20f, 15f, -20.0f, // view point loc
				0.0f, 1.0f, 0.0f); // view-up vector
	}

	private void drawBody(float width, float height, float depth) {

		// the vertices for the cube (note that all sides have a length of 1)
		Vertex v1 = new Vertex(-width, -height, depth);
		Vertex v2 = new Vertex(-width, height, depth);
		Vertex v3 = new Vertex(width, height, depth);
		Vertex v4 = new Vertex(width, -height, depth);
		Vertex v5 = new Vertex(-width, -height, -depth);
		Vertex v6 = new Vertex(-width, height, -depth);
		Vertex v7 = new Vertex(width, height, -depth);
		Vertex v8 = new Vertex(width, -height, -depth);

		// draw the near face:

		GL11.glBegin(GL11.GL_POLYGON);
		{
			GL11.glTexCoord2f(1f, 1f);
			v3.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v2.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v1.submit();
			GL11.glTexCoord2f(0f, 1f);
			v4.submit();
		}
		GL11.glEnd();

		// draw the left face:
		GL11.glBegin(GL11.GL_POLYGON);
		{
			GL11.glTexCoord2f(1f, 1f);
			v2.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v6.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v5.submit();
			GL11.glTexCoord2f(0f, 1f);
			v1.submit();
		}
		GL11.glEnd();

		// draw the right face:
		GL11.glBegin(GL11.GL_POLYGON);
		{
			GL11.glTexCoord2f(1f, 1f);
			v7.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v3.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v4.submit();
			GL11.glTexCoord2f(0f, 1f);
			v8.submit();
		}
		GL11.glEnd();

		// draw the top face:
		GL11.glBegin(GL11.GL_POLYGON);
		{

			GL11.glTexCoord2f(1f, 1f);
			v7.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v6.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v2.submit();
			GL11.glTexCoord2f(0f, 1f);
			v3.submit();
		}
		GL11.glEnd();

		// draw the bottom face:
		GL11.glBegin(GL11.GL_POLYGON);
		{

			GL11.glTexCoord2f(1f, 1f);
			v4.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v1.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v5.submit();
			GL11.glTexCoord2f(0f, 1f);
			v8.submit();
		}
		GL11.glEnd();

		// draw the far face:
		GL11.glBegin(GL11.GL_POLYGON);
		{

			GL11.glTexCoord2f(1f, 1f);

			v6.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v7.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v8.submit();
			GL11.glTexCoord2f(0f, 1f);
			v5.submit();

		}
		GL11.glEnd();
	}
}
