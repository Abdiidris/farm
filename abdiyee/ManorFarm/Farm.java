package coursework.abdiyee.ManorFarm;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import Examples.AnimatedPerson.AnimatedPerson;
import GraphicsLab.*;
/*
 *  Abdiyee Idris
 *  Second Year
 *  180034859
 *  
 *  This is my own work 
 *  
 *  //TODO SCENE GRAPH
 *  
 *  What my program does
 *  --------------------
 *  - It is a fenced plantation where the tractor can harvest the plantation.
 *  - There's also a rain animation which can be turned on
 *  	
 *  
 *  Interaction controls
 *	--------------------
 *	
 *	 		KEY			RESULT
 *		-----------|-----------------------------------------
 *			 H 	   |  Starts harvest animation
 *		 	 R     |  Starts rain animation
 *		 	 K     |  Kills the rain and harvest animations
 *
 *		
 *  Texture credits
 *  ---------------
 * 
 *	 	   Texture       | Source
 *		-----------------|------------------------------------------------------------------------------
 *	     plastic.bmp     | http://metal.graphics/graphic/brushed/green-texture/
 *		 rubber.bmp      | https://www.vectorstock.com/royalty-free-vector/black-rubber-texture-vector-953753
 *		 redPlastic.bmp  | https://www.photos-public-domain.com/2013/11/13/bumpy-red-plastic-texture/
 *		 wood.bmp        | https://www.freepik.com/free-photos-vectors/wood-texture
 *		 dirt.bmp        | https://www.pinterest.com/pin/599260294139161417/
 *		 grass.bmp       | https://www.123rf.com/photo_115582106_green-grass-texture-for-background-green-lawn-pattern-and-texture-background-close-up-.html
 * 					
 * 
 * */

public class Farm extends GraphicsLab {

	Tractor tractor;
	Fence fence;
	Ground ground;
	Rain rain;
	/*
	 * animation variables
	 */

	// records the last destination of the tractor
	float lastXDestination;
	// records whether the truck should go up
	// float zDir = 0.05f; // the initial Z direction for the tractor
	float ANGLE = 180; // the angle at which the truck should be rotated
	boolean STRAIGHT = true;
	boolean HARVEST = false;
	int CURRENTHARVEST = 0;
	boolean RAIN = false;
	public static final float ANIMATIONSCALE =1f;

	public static void main(String args[]) {

		new Farm().run(WINDOWED, "Farm", ANIMATIONSCALE);
	}

	@Override
	protected void initScene() throws Exception {

		// Make the Scene bright
		float globalAmbient[] = {

				0.8f, /* R */
				0.8f, /* G */
				0.8f, /* B */
				1f };
		GL11.glLightModel(GL11.GL_LIGHT_MODEL_AMBIENT, FloatBuffer.wrap(globalAmbient));
		// make the diffuse white
		float diffuse0[] = { 1f, 1f, 1f, 1.0f };
		// make the light slightly yellow SUN
		float ambient0[] = { 0.1f, 0.1f, 0.04f, 1.0f };

		// above the camera
		float position0[] = { 0.0f, 10.0f, 20.0f, 1f };
		// apply the light
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_AMBIENT, FloatBuffer.wrap(ambient0));
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_DIFFUSE, FloatBuffer.wrap(diffuse0));
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_SPECULAR, FloatBuffer.wrap(diffuse0));
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, FloatBuffer.wrap(position0));
		// enable the lighting
		GL11.glEnable(GL11.GL_LIGHT0);

		/**
		 * create the object. Each object requires it's textures Each object also takes
		 * a position for the open GL list
		 **/

		// for the tractor, also enter it's starting podsition
		tractor = new Tractor(1, -13f, 1f, -12f, loadTexture("coursework/abdiyee/ManorFarm/textures/plastic.bmp"),
				loadTexture("coursework/abdiyee/ManorFarm/textures/rubber.bmp"),
				loadTexture("coursework/abdiyee/ManorFarm/textures/redPlastic.bmp"));
			
		
		
		
		
		
		// set the last destination of the tractor to it's newly assigned destination
		lastXDestination = tractor.getX();

		// create the fence object with the
		fence = new Fence(2, loadTexture("coursework/abdiyee/ManorFarm/textures/wood.bmp"));
		ground = new Ground(3, loadTexture("coursework/abdiyee/ManorFarm/textures/dirt.bmp"),
				loadTexture("coursework/abdiyee/ManorFarm/textures/grass.bmp"));
		
		// Initialize the rain and pass the boundaries
		rain = new Rain(-14 * 3, -14 * 3);

	}

	@Override
	protected void setSceneCamera() {
		// call graphiclab camera setup
		super.setSceneCamera();

		// set the camera
		GLU.gluLookAt(0.0f, 10.0f, 20.0f, // viewer location
				0, 0, 0, // view point loc
				0.0f, 1.0f, 0.0f); // view-up vector
	}

	@Override
	protected void checkSceneInput() {
		// if H is pressed, enable the harvest
		if (Keyboard.isKeyDown(Keyboard.KEY_H)) {
			HARVEST = true;
			// if R is pressed enable the rain
		} else if (Keyboard.isKeyDown(Keyboard.KEY_R)) { // check every 1 sec instead

			// only enable the rain if it isnt already raining
			if (!RAIN) {

				RAIN = true;
				// create the standard ambient light // make the scene darker
				float globalAmbient[] = { 0.3f, 0.3f, 0.3f, 1f };
				// submit new the ambience
				GL11.glLightModel(GL11.GL_LIGHT_MODEL_AMBIENT, FloatBuffer.wrap(globalAmbient));
			}

		} else if (Keyboard.isKeyDown(Keyboard.KEY_K)) {

			// if it is raining, also disbale it
			if (RAIN) {

				RAIN = false;
				// reset the lighting by making it brighting
				float globalAmbient[] = { 0.8f, 0.8f, 0.8f, 1f };
				GL11.glLightModel(GL11.GL_LIGHT_MODEL_AMBIENT, FloatBuffer.wrap(globalAmbient));
			}

			// also reset the harvest animation
			resetAnimations();
		}

		// to avoid glitches
	}

	public void resetAnimations() {

		// reset the harvest bool
		HARVEST = false;
		// take the tractor to it's start position
		tractor.reset();
		// all harvest will be undone, so reset this current crop being harvested to 0
		CURRENTHARVEST = 0;
		// the tractor will be facing straight
		STRAIGHT = true;
		// reset the tractors last destination to it's start X
		lastXDestination = tractor.getX();
		
		// reset the angle
		ANGLE = 180;

		// reset the ground
		try {
			// recreate the ground so that is reset
			ground = new Ground(3, loadTexture("coursework/abdiyee/ManorFarm/textures/dirt.bmp"),
					loadTexture("coursework/abdiyee/ManorFarm/textures/grass.bmp"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void nextHarvest() {
		ANGLE = -90; // turn the truck
		// when moving to the next harvest, move horizontally and not vertically
		STRAIGHT = false;
		// remove the current harvest as it's been gathered
		ground.removeHarvest(CURRENTHARVEST);

		// if they've all been harvested then reset the harvest
		if (CURRENTHARVEST == 3) {
			resetAnimations();
		}
	}

	@Override
	protected void updateScene() {

		// if harvest is enabled, start harvest
		if (HARVEST) {

			// if the truck is heading in a straight direction, change the Z
			if (STRAIGHT) {
				// if the tractor has reached the end of the
				if ((tractor.getZ() >= -1f) || (tractor.getZ() <= -14f)) {
					// change its direction to heading away from the Z
					tractor.flipDirection();
					nextHarvest();
				}

				// also reduce the size of the grass being harvested
				ground.reduceHarvest(CURRENTHARVEST, 0.004f * Farm.ANIMATIONSCALE);
				// move the tractor forward
				tractor.moveForward();
				// tractor.setZ(tractor.getZ() + zDir * Farm.ANIMATIONSCALE);
			} else {

				// if the tractor is travelling in the X direction, change the X value of the
				// tractor
				tractor.setX(tractor.getX() + 0.05f * Farm.ANIMATIONSCALE);

				if ((lastXDestination - tractor.getX()) <= -3.5f) // if it's travelled -3.5f then make it straight as it
																	// has reached the next harvest
				{
					// rotate the tractor
					if (tractor.getZ() <= -13f) {
						ANGLE = 180;// if it's already been rotated
					} else {
						ANGLE = 360;
					}

					// if the
					STRAIGHT = true;
					lastXDestination = tractor.getX();

					// make current harvest the next item
					CURRENTHARVEST++;

				}

			}

		}
	}

	@Override
	protected void renderScene() {
		GL11.glDisable(GL11.GL_CULL_FACE); // Otherwise makes the fence see through

		// reduce the size
		GL11.glScaled(0.25, 0.25, 0.25);
		// move the tractor forward and right
		GL11.glTranslatef(25, 0, 25);

		// if the rain it enabled, the call the make it rain function
		if (RAIN) {
			rain.makeItRain();
		}

		GL11.glPushMatrix();
		{
			// scale the tractor
			GL11.glScalef(3, 3, 3);

			// move it to the already set location
			GL11.glTranslatef(tractor.getX(), tractor.getY(), tractor.getZ());
			// also rotate it by the Y axis using the angle already set by update scene
			GL11.glRotatef(ANGLE, 0, 0.1f, 0);

			// draw tractor
			tractor.draw();

		}
		GL11.glPopMatrix();

		// draw the
		fence.draw();

		GL11.glPushMatrix();
		{

			// the ground inside the fence
			GL11.glTranslatef(0, 0, -0.2f);
			// scale the ground
			GL11.glScalef(22f, 1, 21.8f);
			ground.draw();
		}

		GL11.glPopMatrix();

	}

}
