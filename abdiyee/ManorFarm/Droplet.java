package coursework.abdiyee.ManorFarm;

import java.util.Random;

import org.lwjgl.opengl.GL11;

import GraphicsLab.Colour;

public class Droplet extends FarmItem {
	float x;
	float y;
	float z;
	// use to generate a random location
	Random rnd;

	public Droplet(float xBoundary, float zBoundary) {
		super();

		rnd = new Random();

		/*
		 * Use the boundary to randomly generate a position for the rain drop
		 */

		// select a random Y within the bounds
		y = rnd.nextFloat() + rnd.nextInt((int) 80) + 50;
		if (xBoundary < 1) // if the x boundary is - the make it poisitive and change it back so that it
							// can be used to generate a rnd number
		{
			x = rnd.nextFloat() + rnd.nextInt((int) xBoundary * -1);
			x = x * -1;
		} else {
			x = rnd.nextFloat() + rnd.nextInt((int) xBoundary);
		}
		// do the same
		if (zBoundary < 1) {

			z = rnd.nextFloat() + rnd.nextInt((int) zBoundary * -1);
			z = z * -1;
		} else {
			z = rnd.nextFloat() + rnd.nextInt((int) zBoundary);
		}

	
	}
	
	// return Y
	public float getY() {
		return this.y;
	}

	// reduce the Y of the drop
	public void fall() {
		y = y - 1f * Farm.ANIMATIONSCALE;
	}

	@Override
	public void draw() {
		GL11.glPushAttrib(GL11.GL_TEXTURE);
		{
			GL11.glPushMatrix();
			{
				// disabale textures and lighting before drawing rain drop
				GL11.glDisable(GL11.GL_TEXTURE_2D);
				GL11.glDisable(GL11.GL_LIGHTING);
				Colour.BLUE.submit();
				GL11.glTranslatef(x, y, z);
				drawBox(0.15f, 0.15f, 0.15f);

			}
			GL11.glPopMatrix();
		}
		GL11.glPopAttrib();

	}

}
