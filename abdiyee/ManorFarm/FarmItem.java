package coursework.abdiyee.ManorFarm;

import org.lwjgl.opengl.GL11;

import GraphicsLab.Normal;
import GraphicsLab.Vertex;

public abstract class FarmItem {

	int itemNumber;
	
	// assign the give unique number
	public FarmItem(int itemNumber) {
		this.itemNumber = itemNumber;

	}
	
	// create an empty constructor
	public FarmItem() {

	}
	
	// all children must be able to draw
	abstract public void draw();

	public int getNum() {
		return itemNumber;
	}
	
	// this will draw a 6 sided box using the given parameters
	public void drawBox(float width, float height, float depth) {

		// the vertices for the cube (note that all sides have a length of 1)
		Vertex v1 = new Vertex(-width, -height, depth);
		Vertex v2 = new Vertex(-width, height, depth);
		Vertex v3 = new Vertex(width, height, depth);
		Vertex v4 = new Vertex(width, -height, depth);
		Vertex v5 = new Vertex(-width, -height, -depth);
		Vertex v6 = new Vertex(-width, height, -depth);
		Vertex v7 = new Vertex(width, height, -depth);
		Vertex v8 = new Vertex(width, -height, -depth);

		// draw the near face:
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(v3.toVector(), v2.toVector(), v1.toVector(), v4.toVector()).submit();

			GL11.glTexCoord2f(1f, 1f);
			v3.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v2.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v1.submit();
			GL11.glTexCoord2f(0f, 1f);
			v4.submit();
		}
		GL11.glEnd();

		// draw the left face:
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(v2.toVector(), v6.toVector(), v5.toVector(), v1.toVector()).submit();
			GL11.glTexCoord2f(1f, 1f);
			v2.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v6.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v5.submit();
			GL11.glTexCoord2f(0f, 1f);
			v1.submit();
		}
		GL11.glEnd();

		// draw the right face:
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(v7.toVector(), v3.toVector(), v4.toVector(), v8.toVector()).submit();
			GL11.glTexCoord2f(1f, 1f);
			v7.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v3.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v4.submit();
			GL11.glTexCoord2f(0f, 1f);
			v8.submit();
		}
		GL11.glEnd();

		// draw the top face:
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(v7.toVector(), v6.toVector(), v2.toVector(), v3.toVector()).submit();
			GL11.glTexCoord2f(1f, 1f);
			v7.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v6.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v2.submit();
			GL11.glTexCoord2f(0f, 1f);
			v3.submit();
		}
		GL11.glEnd();

		// draw the bottom face:
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(v4.toVector(), v1.toVector(), v5.toVector(), v8.toVector()).submit();
			GL11.glTexCoord2f(1f, 1f);
			v4.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v1.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v5.submit();
			GL11.glTexCoord2f(0f, 1f);
			v8.submit();
		}
		GL11.glEnd();

		// draw the far face:
		GL11.glBegin(GL11.GL_POLYGON);
		{
			new Normal(v6.toVector(), v7.toVector(), v8.toVector(), v5.toVector()).submit();

			GL11.glTexCoord2f(1f, 1f);

			v6.submit();
			GL11.glTexCoord2f(1f, 0.0f);
			v7.submit();
			GL11.glTexCoord2f(0f, 0.0f);
			v8.submit();
			GL11.glTexCoord2f(0f, 1f);
			v5.submit();
		}
		GL11.glEnd();

	}
}
