package coursework.abdiyee.ManorFarm;

import java.util.ArrayList;

public class Rain {
	
	// this will store the droplets
	ArrayList<Droplet> drops;
	
	// the bounds in which the rain drops can be created
	float xBoundary;
	float zBoundary;
	public Rain(float xBoundary,float zBoundary)
	{
		// set the boundaries
		this.xBoundary=xBoundary;
		this.zBoundary=zBoundary;
		

		this.drops=new ArrayList<Droplet>();
		
		// create some raindrops
		geneateDrops();
	}
	
	public void makeItRain()
	{
		
		// reduce the Y of each raindrop and draw
		for (int i = 0; i < drops.size(); i++) {
			
			// move the raindrop down
			drops.get(i).fall();
			drops.get(i).draw();
			
			// if the raindrop has hit the ground, replace it
			if(drops.get(i).getY()<0)
			{
				drops.remove(i);
				drops.add(new Droplet(xBoundary,zBoundary));
			}
			
		}
	}
	public void geneateDrops()
	{
		// add some new droplets to the arraylist
		for (int i = 0; i < 200; i++) {
			this.drops.add(new Droplet(xBoundary,zBoundary));
		}
	}
	
}
